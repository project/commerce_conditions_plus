<?php

declare(strict_types=1);

namespace Drupal\Tests\commerce_conditions_plus\Kernel;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\commerce_price\Price;
use Drupal\commerce_shipping\Entity\Shipment;
use Drupal\commerce_shipping\ShipmentItem;
use Drupal\physical\Weight;
use Drupal\Tests\commerce_order\Kernel\OrderKernelTestBase;

/**
 * The conditions evaluator test.
 *
 * @group commerce_conditions_plus
 * @requires module commerce_shipping
 */
final class ConditionsEvaluatorTest extends OrderKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'physical',
    'commerce_payment',
    'commerce_payment_example',
    'commerce_shipping',
    'commerce_conditions_plus',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['commerce_shipping']);
  }

  /**
   * The conditions evaluate test.
   *
   * @param array $conditions
   *   The conditions.
   * @param bool $expected_result
   *   The expected result.
   *
   * @dataProvider conditionsData
   */
  public function testEvaluate(array $conditions, bool $expected_result) {
    $sut = $this->container->get('commerce_conditions_plus.conditions_evaluator');

    $payment_gateway = PaymentGateway::create([
      'id' => 'example',
      'label' => 'Example',
      'plugin' => 'example_onsite',
    ]);
    $order = Order::create([
      'type' => 'default',
      'total_price' => new Price('10.00', 'USD'),
    ]);
    $shipment = Shipment::create([
      'type' => 'default',
      'order_id' => $order->id(),
      'title' => 'Shipment',
      'tracking_code' => 'ABC123',
      'items' => [
        new ShipmentItem([
          'order_item_id' => 1,
          'title' => 'T-shirt (red, large)',
          'quantity' => 2,
          'weight' => new Weight('2', 'lb'),
          'declared_value' => new Price('1', 'USD'),
        ]),
      ],
      'weight' => new Weight('2', 'lb'),
      'amount' => new Price('5', 'USD'),
      'state' => 'draft',
    ]);

    $payment_gateway->set('conditions', $conditions);

    self::assertEquals(
      $expected_result,
      $sut->execute($payment_gateway->getConditions(), $payment_gateway->getConditionOperator(), [
        'commerce_order' => $order,
        'commerce_shipment' => $shipment,
      ], $payment_gateway)
    );
  }

  /**
   * The conditions data.
   */
  public static function conditionsData(): \Generator {
    yield 'simple condition pass' => [
      [
        [
          'plugin' => 'order_type',
          'configuration' => [
            'bundles' => ['default'],
            'parent' => '',
            'depth' => '',
            'sort_weight' => '0',
          ],
        ],
      ],
      TRUE,
    ];
    yield 'simple condition fail' => [
      [
        [
          'plugin' => 'order_type',
          'configuration' => [
            'bundles' => ['physical'],
            'parent' => '',
            'depth' => '',
            'sort_weight' => '0',
          ],
        ],
      ],
      FALSE,
    ];
    yield 'multiple AND conditions' => [
      [
        [
          'plugin' => 'order_type',
          'configuration' => [
            'bundles' => ['default'],
            'parent' => '',
            'depth' => '',
            'sort_weight' => '0',
          ],
        ],
        [
          'plugin' => 'shipment_weight',
          'configuration' => [
            'operator' => '>',
            'weight' => [
              'number' => '1',
              'unit' => 'lb',
            ],
            'parent' => '',
            'depth' => '',
            'sort_weight' => '0',
          ],
        ],
      ],
      TRUE,
    ];
    yield 'negated condition pass' => [
      [
        [
          'plugin' => 'order_type',
          'configuration' => [
            'bundles' => ['physical'],
            'parent' => '',
            'depth' => '',
            'sort_weight' => '0',
            'negate_condition' => TRUE,
          ],
        ],
      ],
      TRUE,
    ];
  }

}
