<?php

declare(strict_types=1);

namespace Drupal\Tests\commerce_conditions_plus\Kernel;

use Drupal\commerce_conditions_plus\Plugin\Field\FieldWidget\ConditionsTable;
use Drupal\Tests\commerce_order\Kernel\OrderKernelTestBase;

/**
 * The field widget test.
 *
 * @group commerce_conditions_plus
 */
final class FieldWidgetInfoAlterTest extends OrderKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'commerce_promotion',
    'commerce_conditions_plus',
  ];

  /**
   * Tests the widget class is swapped.
   */
  public function testWidgetClassOveridden() {
    $widget_manager = $this->container->get('plugin.manager.field.widget');
    $definition = $widget_manager->getDefinition('commerce_conditions');
    self::assertEquals(ConditionsTable::class, $definition['class']);
  }

}
