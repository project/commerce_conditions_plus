<?php

declare(strict_types=1);

namespace Drupal\Tests\commerce_conditions_plus\FunctionalJavascript;

use Drupal\commerce_promotion\Entity\Promotion;
use Drupal\Tests\commerce\FunctionalJavascript\CommerceWebDriverTestBase;

/**
 * The promotion widget test.
 *
 * @group commerce_conditions_plus
 */
final class PromotionWidgetTest extends CommerceWebDriverTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'commerce_product',
    'commerce_promotion',
    'commerce_conditions_plus',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getAdministratorPermissions() {
    return array_merge([
      'administer commerce_promotion',
      'access commerce_promotion overview',
    ], parent::getAdministratorPermissions());
  }

  /**
   * Tests the new widget.
   */
  public function testWidget() {
    $this->drupalGet('admin/commerce/promotions');
    $this->getSession()->getPage()->clickLink('Add promotion');

    $this->getSession()->getPage()->fillField('name[0][value]', 'PlusConditionsTable');
    $this->getSession()->getPage()->fillField('display_name[0][value]', 'PlusConditionsTable');
    $this->getSession()->getPage()->selectFieldOption('offer[0][target_plugin_id]', 'order_item_percentage_off');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->getSession()->getPage()->fillField('offer[0][target_plugin_configuration][order_item_percentage_off][percentage]', '10.0');

    $this->htmlOutput();
    $conditions_widget_add_new = $this->getSession()->getPage()->find('css', '#edit-conditions-form-add-new');
    $conditions_widget_add_new->selectFieldOption('conditions[form][add_new][conditions_id]', 'order_total_price');
    $conditions_widget_add_new->pressButton('Add');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->assertSession()->fieldExists('conditions[form][0][negate_condition]');
    $this->assertSession()->checkboxNotChecked('conditions[form][0][negate_condition]');
    $this->getSession()->getPage()->selectFieldOption('conditions[form][0][configuration][form][operator]', '>');
    $this->getSession()->getPage()->fillField('conditions[form][0][configuration][form][amount][number]', '25');
    $this->htmlOutput();

    // Add the same condition twice and make sure negate is not checked.
    $this->getSession()->getPage()->find('css', '#edit-conditions-form-add-new');
    $this->getSession()->getPage()->selectFieldOption('conditions[form][add_new][conditions_id]', 'order_total_price');
    $this->getSession()->getPage()->pressButton('Add');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->assertSession()->fieldExists('conditions[form][1][negate_condition]');
    $this->assertSession()->checkboxNotChecked('conditions[form][1][negate_condition]');
    $this->getSession()->getPage()->checkField('conditions[form][1][negate_condition]');
    $this->getSession()->getPage()->selectFieldOption('conditions[form][1][configuration][form][operator]', '>');
    $this->getSession()->getPage()->fillField('conditions[form][1][configuration][form][amount][number]', '100');
    $this->setRawFieldValue('start_date[0][value][date]', '2019-11-29');
    $this->setRawFieldValue('start_date[0][value][time]', '10:30:00');
    $this->submitForm([], t('Save'));

    $promotion = Promotion::load(1);
    $conditions = $promotion->get('conditions')->getValue();
    $this->assertEquals([
      [
        'target_plugin_id' => 'order_total_price',
        'target_plugin_configuration' => [
          'amount' => [
            'number' => '25',
            'currency_code' => 'USD',
          ],
          'operator' => '>',
          'parent' => '',
          'depth' => 0,
          'sort_weight' => '0',
          'negate_condition' => 0,
        ],
      ],
      [
        'target_plugin_id' => 'order_total_price',
        'target_plugin_configuration' => [
          'amount' => [
            'number' => '100',
            'currency_code' => 'USD',
          ],
          'operator' => '>',
          'parent' => '',
          'depth' => 0,
          'sort_weight' => '1',
          'negate_condition' => 1,
        ],
      ],
    ], $conditions);
  }

}
