<?php

declare(strict_types=1);

namespace Drupal\commerce_conditions_plus\Plugin\Field\FieldWidget;

use Drupal\commerce\Plugin\Field\FieldWidget\ConditionsWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'commerce_conditions' widget.
 *
 * @FieldWidget(
 *   id = "commerce_conditions_plus_conditions_table",
 *   label = @Translation("Conditions Table"),
 *   field_types = {
 *     "commerce_plugin_item:commerce_condition"
 *   },
 *   multiple_values = TRUE
 * )
 */
class ConditionsTable extends ConditionsWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['form']['#type'] = 'commerce_conditions_table';
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // AND and OR operators have no configuration, so when they are submitted
    // the form has no `configuration` array. We add it in so that the parent
    // massageFormValues passes.
    // @todo Upstream should support configurationless conditions (albeit rare.)
    foreach ($values['form'] as $key => $value) {
      if (!isset($value['plugin'])) {
        continue;
      }
      if (empty($value['configuration'])) {
        $values['form'][$key]['configuration'] = [];
      }
    }
    return parent::massageFormValues($values, $form, $form_state);
  }

}
