<?php

declare(strict_types=1);

namespace Drupal\commerce_conditions_plus\Plugin\Commerce\Condition;

use Drupal\commerce\Plugin\Commerce\Condition\ConditionBase;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides OR operator for conditions.
 *
 * @CommerceCondition(
 *   id = "commerce_conditions_plus_and_operator",
 *   label = @Translation("And Operator"),
 *   category = @Translation("Conditions Plus"),
 *   entity_type = "commerce_order",
 * )
 */
final class AndOperator extends ConditionBase {

  /**
   * Evaluates conditions.
   */
  public function evaluate(EntityInterface $entity) {
    // @todo find child via config? evaluate there?
    return TRUE;
  }

}
