<?php

declare(strict_types=1);

namespace Drupal\commerce_conditions_plus\Entity;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\Entity\ShippingMethod as BaseShippingMethod;

/**
 * Extends the shipping method entity class.
 */
class ShippingMethod extends BaseShippingMethod {

  /**
   * {@inheritdoc}
   */
  public function applies(ShipmentInterface $shipment) {
    $sut = \Drupal::getContainer()->get('commerce_conditions_plus.conditions_evaluator');
    return $sut->execute($this->getConditions(), $this->getConditionOperator(), [
      'commerce_order' => $shipment->getOrder(),
      'commerce_shipment' => $shipment,
    ]);
  }

}
