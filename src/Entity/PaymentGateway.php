<?php

declare(strict_types=1);

namespace Drupal\commerce_conditions_plus\Entity;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentGateway as BasePaymentGateway;

/**
 * Extends the payment gateway entity class.
 */
class PaymentGateway extends BasePaymentGateway {

  /**
   * {@inheritdoc}
   */
  public function applies(OrderInterface $order) {
    $sut = \Drupal::getContainer()->get('commerce_conditions_plus.conditions_evaluator');
    return $sut->execute($this->getConditions(), $this->getConditionOperator(), [
      'commerce_order' => $order,
    ]);
  }

}
